class Ennemy {
    constructor(name, picture, x, y, width, height, frameX, frameY, speed, direction, moving, point) {
        this.name = name;
        this.picture = picture;
        this.x = Math.random()*750;
        this.y = y;
        this.width = width;
        this.height = height;
        this.frameX = frameX;
        this.frameY = frameY;
        this.speed = speed;
        this.direction = direction;
        this.moving = moving;
        this.point = -5;
    }
    
    draw(ctx)
    {
        ctx.drawImage(this.picture, this.width * this.frameX, this.height * this.frameY, this.width, this.height, this.x, this.y, this.width, this.height);
        this.handleFrame();
    }
    handleFrame() {
        if (this.frameX < 3 && this.moving) this.frameX++
        else this.frameX = 0;
    }

    //***********************************************************
    //  SECTION MOUVEMENT ENNEMY
    //***********************************************************
    MoveLeftToRightAndBack(player)
    {
        // Move x => en horizontal
        this.moving = true;
        this.x += this.speed * this.direction; 
        
        // Calcul OutOfCanvas horizontal
        let centralPoint = this.x + this.width / 2;
        let isOutOfCanvasFromLeft = centralPoint <= 0;
        let isOutOfCanvasFromRight = centralPoint >= gameCanvas.width;
        
        // Check contraintes
        if (isOutOfCanvasFromLeft || isOutOfCanvasFromRight) 
        {
            // Changement de direction
            this.direction = -this.direction;
            
            // Frame allée ou frame retour
            if (this.direction == 1) this.frameY = 2;		
            else this.frameY = 1;
        }

        if(this.checkCollision(player))
        {
            this.x = 0 + this.width;
            this.y =  Math.random()* (gameCanvas.height - this.height)
        }
    }

    MoveLeftToRight(player) {
        this.moving = true;
        this.x += this.speed;
        this.frameY = 2;
        
        // Calcul OutOfCanvas horizontal
        let centralPoint = this.x + this.width / 2;
        let isOutOfCanvasFromRight = centralPoint >= gameCanvas.width;
        
        // Check contraintes pour repositionnement
        if (isOutOfCanvasFromRight) { this.x = 0; }

        if(this.checkCollision(player))
        {
             this.x = 0 + this.width;
             this.y =  Math.random()* (gameCanvas.height - this.height)
            
        }
    }

    MoveRightToLeft(player) {
        this.moving = true;
        this.x -= this.speed
        this.frameY = 1;        

        // Calcul OutOfCanvas horizontal
        let centralPoint = this.x + this.width / 2;
        let isOutOfCanvasFromLeft = centralPoint <= 0;

        // Check contraintes pour repositionnement
        if (isOutOfCanvasFromLeft) { this.x = gameCanvas.width; }

        if(this.checkCollision(player))
        {
            this.x = gameCanvas.width + this.width;
            this.y =  Math.random()* (gameCanvas.height - this.height)

                
        }
    }

    MoveUpToDownAndBack(player)
    {
        // Move y => en vertical
        this.moving = true;
        this.y += this.speed * this.direction;

        
        let centralPoint = this.y + this.height / 2;

        // Calcul OutOfCanvas vertical
        let isOutOfCanvasFromUp = centralPoint <=0;
        let isOutOfCanvasFromDown = centralPoint >= gameCanvas.height;
        

        // Check contraintes
        if (isOutOfCanvasFromUp || isOutOfCanvasFromDown)
        {
            // Changement de direction
            this.direction = -this.direction;
            
            // Frame aller ou frame retour
            if (this.direction == 1) this.frameY = 0;
            else this.frameY = 3;
        }

        if(this.checkCollision(player))
        {
            this.y = 150;
            this.x = Math.random()* gameCanvas.height
        }
    }
    
    //***********************************************************
    // Collision + TEXT ALERT 
    //***********************************************************
    randomSoundEnemy() {
        let alertSound = new Array (
        "sound/thomasVoice/enemy/chier.mp3",
        "sound/thomasVoice/enemy/comment.mp3",
        "sound/thomasVoice/enemy/deteste.mp3",
        "sound/thomasVoice/enemy/interet.mp3",
        "sound/thomasVoice/enemy/plus2.mp3",
        "sound/thomasVoice/enemy/putain.mp3",
        "sound/thomasVoice/enemy/putain2.mp3",

        );

        let i = Math.floor(Math.random()*alertSound.length);

        let srcSound = alertSound[i]
        let soundEnemy = document.createElement('audio');
        soundEnemy.src = srcSound;
        soundEnemy.play();

    }
    
    checkCollision(player)
    {
        if (player.x < this.x + this.width &&
            player.x + player.width > this.x &&
            player.y < this.y + this.height &&
            player.height + player.y > this.y) 
            {        
                player.life += this.point;
                this.randomSoundEnemy()
                return true;
            }
            else return false;
    }
}