//***********************************************************
// Canvas
//***********************************************************
const gameCanvas = document.getElementById("gameCanvas");
const ctx = gameCanvas.getContext('2d');
gameCanvas.width = 800;
gameCanvas.height = 500;

//***********************************************************
// Instances objets de différents types
// params = name, picture, x, y, width, height, frameX, frameY, speed, direction, moving
//***********************************************************


function createImage(src) {
    return new Promise((imageLoaded) => {
        const image = new Image();
        image.src = src
        image.onload = () => imageLoaded(image)
    })
}

createImage('image/back.jpg').then(async (background) => {
    const hulk = new Ennemy('hulk', await createImage('image/hulk.png'), 0, 250, 40, 56, 0, 2, 5, 1, false);
    const ironMan = new Ennemy('ironMan', await createImage('image/ironman.png'), 0, 300, 32, 48, 0, 2, 3, 1, false);
    const captain = new Ennemy('captain', await createImage('image/captain.png'), 0, 400, 32, 48, 0, 2, 4, 1, false);
    const thor = new Ennemy('thor', await createImage('image/thor.png'), 0, 200, 32, 48, 0, 0, 3, 1, false,);
    const drax = new Ennemy('drax', await createImage('image/drax.png'), 0, 420, 40, 56, 0, 2, 3, 1, false);
    const deadpool = new Ennemy('deadpool', await createImage('image/deadpool.png'), 0, 450, 32, 48, 0, 0, 4, 1, false);
    const thomas = new Player('thomas', await createImage('image/thomas.png'), 32, 48, 30, 100, 3, 0, 10, false);

    let frame = 0;
    const beerArray = [];
    const beer = new Beer(this.x, this.y, this.width, this.height, this.frameX, this.frameY, this.speed, this.point)

    function randomBeer() {
        if (frame % 50 === 0) {
            beerArray.push(new Beer())
        }
        for (let i = 0; i < beerArray.length; i++) {
            beerArray[i].updatePosition(thomas);
            beerArray[i].draw()
        }
    }

    function drawBeers() {
        beer.draw();
        beer.updatePosition(thomas);
        randomBeer();
        frame++;
    }

    //***********************************************************
    // Event clavier du player
    //***********************************************************
    let keys = [];

    window.addEventListener('keydown', function (e) {
        keys[e.keyCode] = true;
        thomas.moving = true;
    })

    window.addEventListener('keyup', function (e) {
        delete keys[e.keyCode]
        thomas.moving = false;
    })

    //***********************************************************
    // SECTION HTML Score combat
    //***********************************************************

    let scoreProgress = document.getElementById("progress");

    function txtHtmlScore() {
        scoreProgress.value = thomas.life
    }

    //***********************************************************
    // SECTION Audio
    //***********************************************************

    let backgroundMusic = document.createElement('audio');
    backgroundMusic.setAttribute('src', 'sound/game.mp3');

    let continueMusic = document.createElement('audio');
    continueMusic.setAttribute('src', 'sound/continue.mp3');

    let youWin = document.createElement('audio');
    youWin.setAttribute('src', 'sound/youwin.mp3');

    let youLose = document.createElement('audio');
    youLose.setAttribute('src', 'sound/youlose.mp3');

    function cedricSound(){
    let cedric = document.createElement('audio');
    cedric.setAttribute('src', 'sound/thomasVoice/cedric.mp3');
    cedric.play()
    }



    let muteMusicButton = document.getElementById("muteIcon")
    let unmuteMusicButton = document.getElementById("unmuteIcon")


    muteMusicButton.addEventListener("mousedown", () => {
        muteMusicButton.classList.add("hidden");   
        unmuteMusicButton.classList.remove("hidden");  
        backgroundMusic.pause();
    })

    unmuteMusicButton.addEventListener("mousedown", () => {
        unmuteMusicButton.classList.add("hidden");   
        muteMusicButton.classList.remove("hidden");    
        backgroundMusic.play();  
    })


    //****************************************
    //  SECTION CHRONO
    //****************************************
     
    let start = 0;
    let end = 0;
    let diff = 0;
    let timerID = 0;

        let chronometre = true
    function chrono() {
        if(chronometre == true){

       
        end = new Date();
        diff = end - start;
        diff = new Date(diff);

        let msec = diff.getMilliseconds()
        let sec = diff.getSeconds();
        let min = diff.getMinutes();
        if (min < 10) {
            min = "0" + min
        }
        if (sec < 10) {
            sec = "0" + sec
        }
        document.getElementById("chronotime").innerHTML =  min + ":" + sec + ":" + msec;

        
        document.getElementById("yourTime").innerHTML = min + ":" + sec + ":" + msec;

        timerID = setInterval(() => {
            chrono()
        },10 );
       } 
    }

    function chronoStart() {
        start = new Date()
        chrono()
    }
    function chronoContinue() {
        start = new Date() - diff
        start = new Date(start)
        chrono()
    }
   
    //***********************************************************
    // Gestion du processus du jeu
    // Start Game and Game Over
    //***********************************************************

    // Bouton HTML start
    const startButton = document.getElementById("start");

    // Déroulement d'une party
    let animating = true
    function letsPlay() {

        if (animating == true) {

            // Refresh background
            ctx.drawImage(background, 0, 0, gameCanvas.width, gameCanvas.height);

            // Manage player
            thomas.draw(ctx, keys);

            // les bières
            drawBeers();

            //-------------------
            // Manage Ennemies
            //-------------------
            hulk.draw(ctx);
            hulk.MoveLeftToRightAndBack(thomas);
            //-------------------
            thor.draw(ctx);
            thor.MoveUpToDownAndBack(thomas);
            //-------------------
            ironMan.draw(ctx);
            ironMan.MoveRightToLeft(thomas);
            //-------------------
            captain.draw(ctx);
            captain.MoveLeftToRight(thomas);
            //-------------------
            drax.draw(ctx);
            drax.MoveRightToLeft(thomas);
            //-------------------
            deadpool.draw(ctx);
            deadpool.MoveUpToDownAndBack(thomas); // Si collision : player.life - ennemy.point;

            // Affichage du score
            txtHtmlScore();

            // Vérifie si la partie continue ou s'arrête
            checkGameOverAndStop();
            requestAnimationFrame(letsPlay);
        }
    }

    let gameStarted = false;

    // Action Start d'une 'session de jeu' 
    function startBtnGame() {
          
            chronoStart();
            chronoContinue()
            letsPlay()
            backgroundMusic.play();
            startButton.style.visibility = 'hidden'
        
    }

    startButton.addEventListener("click", startBtnGame);

   
    // Action Stop de la 'session de jeu'
    function checkGameOverAndStop() {
        
        if (thomas.life <= 0) {
            chronometre = false
            animating = false;
            backgroundMusic.pause();
            continueMusic.play();
            youLose.play();
            displayDefeatDiv();


        }

        if (thomas.life >= thomas.lifeMax) {
            chronometre = false
            animating = false;
            backgroundMusic.pause()
            continueMusic.play();
            youWin.play();
            displayVictoryDiv();
            

            setTimeout(() => {
                cedricSound()
            }, 1000);
           

        }
    }

    //***********************************************************
    // DIV Victoire et Défaite
    //***********************************************************

    const victoryDiv = document.getElementById("victoryDiv")
    const defeatDiv = document.getElementById("defeatDiv")

    function displayVictoryDiv() {
        victoryDiv.classList.add("visible")
    }

    function displayDefeatDiv() {
        defeatDiv.classList.add("visible")
    }


})