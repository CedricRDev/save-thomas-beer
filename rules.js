

    //***********************************************************
    // SECTION Règles
    //***********************************************************

    const rulesButton = document.getElementById("rules")
    const rulesDiv = document.getElementById("rulesDiv")
    const closeRulesButton = document.getElementById("closeRules")

    
    function displayRulesDiv() {
        rulesDiv.classList.add("visible")
    }

    function closeRulesDiv() {
        rulesDiv.classList.remove("visible")
    }

    rulesButton.addEventListener("click", () => {
        displayRulesDiv()
    })

    closeRulesButton.addEventListener("click", () => {
        closeRulesDiv()
    })
