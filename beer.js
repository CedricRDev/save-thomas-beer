class Beer {
    constructor (x,y,width, height, frameX, frameY, speed, point){
        this.x = Math.random() * gameCanvas.width  ;
        this.y = 100;
        this.width = 26;
        this.height = 20;
        this.frameX = 0;
        this.frameY = 0;
        this.speed = Math.random() * 5 +2
        this.point = 10;
    }

    updatePosition(player){
        this.y += this.speed
            if (this.checkCollision(player)) {
                this.x = Math.random() * gameCanvas.width - this.width ;
                this.y = 150;
        }
    }

    draw(){
        const beerPicture = new Image();
        beerPicture.src = 'image/beer.png';
        ctx.drawImage(beerPicture,0,0,this.width,this.height,this.x,this.y, this.width,this.height);
    } 
    
    randomSoundBeer() {
        let alertSound = new Array (
        "sound/thomasVoice/beer/genial.mp3",
        "sound/thomasVoice/beer/incroyable.mp3",
        "sound/thomasVoice/beer/magnifique.mp3",
        "sound/thomasVoice/beer/plus.mp3",
        "sound/thomasVoice/beer/vie.mp3"
        );

        let i = Math.floor(Math.random()*alertSound.length);

        let srcSound = alertSound[i]
        let soundBeer = document.createElement('audio');
        soundBeer.src = srcSound;
        soundBeer.play();

    }
    
  
    checkCollision(player) 
    {
        if (player.x < this.x + this.width &&
            player.x + player.width > this.x &&
            player.y < this.y + this.height &&
            player.height + player.y > this.y) 
            {        
                player.life += this.point;
                this.randomSoundBeer()
                return true;
            }
            else return false;
    }  
}