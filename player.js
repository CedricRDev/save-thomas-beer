class Player {
    constructor ( name , picture, width, height, life, lifeMax, frameX, frameY, speed, moving ){
        this.name =  name;
        this.picture = picture;
        this.x = 50;
        this.y = 20;
        this.width = width;
        this.height = height;
        this.life = life;
        this.lifeMax = lifeMax
        this.frameX = frameX;
        this.frameY = frameY;
        this.speed = speed;
        this.moving = moving;
    }

    draw(ctx,keys)
    {
        this.move(keys)
        ctx.drawImage(this.picture, this.width * this.frameX, this.height * this.frameY, this.width, this.height, this.x, this.y, this.width, this.height);
        
            //handleFrame
        if (this.frameX < 3 && this.moving) this.frameX++
        else this.frameX = 0;
    }

    // Mouvement du Player
    move(keys)
    {        
        //bouge a gauche
        if (keys[37] && this.x > 0){
            this.x -= this.speed;
            this.frameY = 1;
            this.moving = true;
        }

        //bouge en haut
        if (keys[38] && this.y > 0 ){
            this.y -= this.speed;
            this.frameY = 3;
            this.moving = true;             
        }

        //bouge a droite
        if (keys[39] && this.x < gameCanvas.width - this.width){
            this.x += this.speed;
            this.frameY =2;
            this.moving = true;
        }

        //bouge en bas
        if (keys[40] && this.y < gameCanvas.height - this.height){
            this.y += this.speed;
            this.frameY = 0;
            this.moving = true;             
        }
    }
}